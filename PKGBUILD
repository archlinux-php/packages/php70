# Maintainer: Michal Sotolar <michal@sotolar.com>
# Credit goes also to the maintainers and contributors of other PHP versions in
# AUR or official Arch Linux repositories. Specific patches might include code
# from other open source projects. In that case, credit is attributed in the
# specific commit.

pkgbase=php70
_pkgbase=${pkgbase%70}

pkgname=("${pkgbase}"
         "${pkgbase}-cgi"
         "${pkgbase}-apache"
         "${pkgbase}-fpm"
         "${pkgbase}-embed"
         "${pkgbase}-phpdbg"
         "${pkgbase}-dblib"
         "${pkgbase}-enchant"
         "${pkgbase}-gd"
         "${pkgbase}-imap"
         "${pkgbase}-intl"
         "${pkgbase}-mcrypt"
         "${pkgbase}-odbc"
         "${pkgbase}-pgsql"
         "${pkgbase}-pspell"
         "${pkgbase}-snmp"
         "${pkgbase}-sqlite"
         "${pkgbase}-tidy"
         "${pkgbase}-xsl")

pkgver=7.0.33
_pkgver=${pkgbase//php}
pkgrel=9
pkgdesc="A general-purpose scripting language that is especially suited to web development"
arch=('x86_64')
url='http://www.php.net'
license=('PHP')

makedepends=('apache' 'aspell' 'db' 'enchant' 'gd' 'gmp' 'icu' 'libmcrypt' 
             'libxslt' 'libzip' 'net-snmp' 'postgresql-libs' 'sqlite' 'systemd'
             'tidy' 'unixodbc' 'curl' 'libtool' 'smtp-forwarder' 'freetds' 'pcre'
             'c-client')

source=("https://php.net/distributions/${_pkgbase}-${pkgver}.tar.xz"
        "https://php.net/distributions/${_pkgbase}-${pkgver}.tar.xz.asc"
        'apache.patch' 'apache.conf' 'enchant-2.patch' 'freetype2.patch'
        'php-fpm.patch' 'php-fpm.tmpfiles' 'php.ini.patch' 'icu-70.patch'
        'php-icu-1100-Utilize-the-recommended-way-to-handle-the-icu-namespace.patch'
        'php-icu-1101-Simplify-namespace-access.patch')

sha256sums=('ab8c5be6e32b1f8d032909dedaaaa4bbb1a209e519abb01a52ce3914f9a13d96'
            'SKIP'
            '819f05d2fd5a75c96e93c863517ca77dbd021a1224dc2d8096f758fb2937df6a'
            'df075b89484eb3a08402788580de16d23123f95541b2e9aed8d928105de9b874'
            'da784a8fc52285e64d88ca40cd9d71b31074a5644827fcabd33dcc1f8da5ed3f'
            '82482323b1bcd18901b1fc3a25efd75f03237d02911a4c5aea5bfae108f3e815'
            '75322f9b38c5961faddcb4f5333fbcb5ea5955e4f54f1eec223fc4367eb3b247'
            'b6b7f3ced56b75bf95513a46f43cde41fc28da714f5e0ed181caf2266f2f0c27'
            '2f678d039313ee613d59c8b4bf9f48068085df0fa8ac7ca4cf807e168061a8c9'
            '5dbc2d5f3bcfe52d9717d0c9ad42f13f6a56917628afc932c6715e2767b7033f'
            '1ab38d1bb19f82b1be480dd89839de2fda79131a6e22c21d91c94ed13f3d11f8'
            '089d3187cefd6d5339c36c0d83abf8340ab133381625c8bbd214929b5dec4073')

validpgpkeys=('1A4E8B7277C42E53DBA9C7B9BCAA30EA9C0D5763'
              '6E4F6AB321FDC07F2C332E3AC2BF0BC433CFC8B3')

prepare() {
        cd ${srcdir}/${_pkgbase}-${pkgver}

        patch -p0 -i "${srcdir}/apache.patch"
        patch -p0 -i "${srcdir}/enchant-2.patch"
        patch -p0 -i "${srcdir}/freetype2.patch"
        patch -p0 -i "${srcdir}/php-fpm.patch"
        patch -p1 -i "${srcdir}/icu-70.patch"
        patch -p1 -i "${srcdir}/php-icu-1100-Utilize-the-recommended-way-to-handle-the-icu-namespace.patch"
        patch -p1 -i "${srcdir}/php-icu-1101-Simplify-namespace-access.patch"
        patch -p0 -i "${srcdir}/php.ini.patch"
}

build() {
        CPPFLAGS+=' -DU_DEFINE_FALSE_AND_TRUE=1'

        local _phpconfig=" \
                --srcdir=../${_pkgbase}-${pkgver} \
                --config-cache \
                --prefix=/usr \
                --sbindir=/usr/bin \
                --sysconfdir=/etc/${pkgbase} \
                --localstatedir=/var \
                --libdir=/usr/lib/${pkgbase} \
                --datarootdir=/usr/share/${pkgbase} \
                --datadir=/usr/share/${pkgbase} \
                --program-suffix=${pkgbase#php} \
                --with-layout=GNU \
                --with-config-file-path=/etc/${pkgbase} \
                --with-config-file-scan-dir=/etc/${pkgbase}/conf.d \
                --disable-rpath \
                --without-pear \
                "

        local _phpextensions="\
                --enable-bcmath=shared \
                --enable-calendar=shared \
                --enable-dba=shared \
                --enable-exif=shared \
                --enable-ftp=shared \
                --enable-gd-native-ttf \
                --enable-intl=shared \
                --enable-mbstring \
                --enable-shmop=shared \
                --enable-soap=shared \
                --enable-sockets=shared \
                --enable-sysvmsg=shared \
                --enable-sysvsem=shared \
                --enable-sysvshm=shared \
                --enable-zip=shared \
                --with-bz2=shared \
                --with-curl=shared \
                --with-db4=/usr \
                --with-enchant=shared,/usr \
                --with-freetype-dir=/usr \
                --with-gd=shared,/usr \
                --with-gdbm \
                --with-gettext=shared \
                --with-gmp=shared \
                --with-iconv=shared \
                --with-imap-ssl \
                --with-imap=shared \
                --with-kerberos=/usr \
                --with-ldap=shared \
                --with-ldap-sasl \
                --with-libzip \
                --with-mcrypt=shared \
                --with-mhash \
                --with-mysql-sock=/run/mysqld/mysqld.sock \
                --with-mysqli=shared,mysqlnd \
                --with-openssl \
                --with-pcre-regex=/usr \
                --with-pdo-dblib=shared,/usr \
                --with-pdo-mysql=shared,mysqlnd \
                --with-pdo-odbc=shared,unixODBC,/usr \
                --with-pdo-pgsql=shared \
                --with-pdo-sqlite=shared,/usr \
                --with-pgsql=shared \
                --with-pspell=shared \
                --with-readline \
                --with-snmp=shared \
                --with-sqlite3=shared,/usr \
                --with-tidy=shared \
                --with-unixODBC=shared,/usr \
                --with-xmlrpc=shared \
                --with-xsl=shared \
                --with-zlib \
                --enable-pcntl \
                "

        export EXTENSION_DIR="/usr/lib/${pkgbase}/modules"

        # php
        mkdir -p "${srcdir}/build"
        cd "${srcdir}/build"
        ln -sf ../${_pkgbase}-${pkgver}/configure
        ./configure ${_phpconfig} \
                --enable-cgi \
                --enable-fpm \
                --with-fpm-systemd \
                --with-fpm-acl \
                --with-fpm-user=http \
                --with-fpm-group=http \
                --enable-embed=shared \
                ${_phpextensions}
        make

        # apache
        cp -Ta ${srcdir}/build ${srcdir}/build-apache
        cd ${srcdir}/build-apache
        ./configure ${_phpconfig} \
                --with-apxs2 \
                ${_phpextensions}
        make

        # phpdbg
        cp -Ta ${srcdir}/build ${srcdir}/build-phpdbg
        cd ${srcdir}/build-phpdbg
        ./configure ${_phpconfig} \
                --enable-phpdbg \
                ${_phpextensions}
        make
}

check() {
        cd "${srcdir}/${_pkgbase}-${pkgver}"

        # Check if sendmail was configured correctly (FS#47600)
        "${srcdir}"/build/sapi/cli/php -n -r 'echo ini_get("sendmail_path");' | grep -q 'sendmail'

        export REPORT_EXIT_STATUS=1
        export NO_INTERACTION=1
        export SKIP_ONLINE_TESTS=1
        export SKIP_SLOW_TESTS=1

        "${srcdir}"/build/sapi/cli/php -n run-tests.php -n -P {tests,Zend}
}

package_php70() {
        pkgdesc='A general-purpose scripting language that is especially suited to web development'
        depends=('pcre' 'libxml2' 'curl' 'libzip')
        provides=("${_pkgbase}=$pkgver")
        backup=("etc/${pkgbase}/php.ini")

        cd ${srcdir}/build
        make -j1 INSTALL_ROOT=${pkgdir} install-{modules,cli,build,headers,programs,pharcmd}

        install -D -m644 ${srcdir}/${_pkgbase}-${pkgver}/php.ini-production ${pkgdir}/etc/${pkgbase}/php.ini
        install -d -m755 ${pkgdir}/etc/${pkgbase}/conf.d/

        rm -f ${pkgdir}/usr/lib/${pkgbase}/modules/*.a
        rm -f ${pkgdir}/usr/lib/${pkgbase}/modules/{enchant,gd,imap,intl,mcrypt,odbc,pdo_dblib,pdo_odbc,pgsql,pdo_pgsql,pspell,snmp,sqlite3,pdo_sqlite,tidy,xsl}.so

        rmdir ${pkgdir}/usr/include/php/include
        mv ${pkgdir}/usr/include/php ${pkgdir}/usr/include/${pkgbase}

        rm ${pkgdir}/usr/bin/phar
        ln -sf phar.${pkgbase/php/phar} ${pkgdir}/usr/bin/${pkgbase/php/phar}

        mv ${pkgdir}/usr/bin/phar.{phar,${pkgbase/php/phar}}
        mv ${pkgdir}/usr/share/man/man1/{phar,${pkgbase/php/phar}}.1
        mv ${pkgdir}/usr/share/man/man1/phar.{phar,${pkgbase/php/phar}}.1

        sed -i "/^includedir=/c \includedir=/usr/include/${pkgbase}" ${pkgdir}/usr/bin/${pkgbase/php/phpize}
        sed -i "/^include_dir=/c \include_dir=/usr/include/${pkgbase}" ${pkgdir}/usr/bin/${pkgbase/php/php-config}
        sed -i "/^\[  --with-php-config=/c \[  --with-php-config=PATH  Path to php-config [${pkgbase/php/php-config}]], ${pkgbase/php/php-config}, no)" ${pkgdir}/usr/lib/${pkgbase}/build/phpize.m4
}

package_php70-cgi() {
        pkgdesc='CGI and FCGI SAPI for PHP'
        depends=("${pkgbase}")
        provides=("${_pkgbase}-cgi=$pkgver")

        cd ${srcdir}/build
        make -j1 INSTALL_ROOT=${pkgdir} install-cgi
}

package_php70-apache() {
        pkgdesc='Apache SAPI for PHP'
        depends=("${pkgbase}" 'apache')
        backup=("etc/httpd/conf/extra/${pkgbase}_module.conf")

        install -D -m755 ${srcdir}/build-apache/libs/libphp7.so ${pkgdir}/usr/lib/httpd/modules/lib${pkgbase}.so
        install -D -m644 ${srcdir}/apache.conf ${pkgdir}/etc/httpd/conf/extra/${pkgbase}_module.conf
}

package_php70-fpm() {
        pkgdesc='FastCGI Process Manager for PHP'
        depends=("${pkgbase}" 'systemd')
        backup=("etc/${pkgbase}/php-fpm.conf" "etc/${pkgbase}/php-fpm.d/php-fpm.conf")
        options=('!emptydirs')

        cd ${srcdir}/build
        make -j1 INSTALL_ROOT=${pkgdir} install-fpm

        install -D -m644 ${srcdir}/build/sapi/fpm/php-fpm.service ${pkgdir}/usr/lib/systemd/system/${pkgbase}-fpm.service
        install -D -m644 ${srcdir}/php-fpm.tmpfiles ${pkgdir}/usr/lib/tmpfiles.d/${pkgbase}-fpm.conf
}

package_php70-embed() {
        pkgdesc='Embedded PHP SAPI library'
        depends=("${pkgbase}" 'libsystemd')
        provides=("${_pkgbase}-embed=$pkgver")
        options=('!emptydirs')

        cd ${srcdir}/build
        make -j1 INSTALL_ROOT=${pkgdir} PHP_SAPI=embed install-sapi
        mv ${pkgdir}/usr/lib/libphp7.so ${pkgdir}/usr/lib/libphp-${_pkgver}.so
}

package_php70-phpdbg() {
        pkgdesc='Interactive PHP debugger'
        depends=("${pkgbase}")
        provides=("${_pkgbase}-phpdbg=$pkgver")
        options=('!emptydirs')

        cd ${srcdir}/build-phpdbg
        make INSTALL_ROOT=${pkgdir} install-phpdbg
}

package_php70-dblib() {
        pkgdesc='dblib module for PHP'
        depends=("${pkgbase}" 'freetds')
        provides=("${_pkgbase}-dblib=$pkgver")

        install -D -m755 ${srcdir}/build/modules/pdo_dblib.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_dblib.so
}

package_php70-enchant() {
        pkgdesc='enchant module for PHP'
        depends=("${pkgbase}" 'enchant')
        provides=("${_pkgbase}-enchant=$pkgver")

        install -D -m755 ${srcdir}/build/modules/enchant.so ${pkgdir}/usr/lib/${pkgbase}/modules/enchant.so
}

package_php70-gd() {
        pkgdesc='gd module for PHP'
        depends=("${pkgbase}" 'gd')
        provides=("${_pkgbase}-gd=$pkgver")

        install -D -m755 ${srcdir}/build/modules/gd.so ${pkgdir}/usr/lib/${pkgbase}/modules/gd.so
}

package_php70-imap() {
        pkgdesc='imap module for PHP'
        depends=("${pkgbase}" 'c-client')
        provides=("${_pkgbase}-imap=$pkgver")

        install -D -m755 ${srcdir}/build/modules/imap.so ${pkgdir}/usr/lib/${pkgbase}/modules/imap.so
}

package_php70-intl() {
        pkgdesc='intl module for PHP'
        depends=("${pkgbase}" 'icu')
        provides=("${_pkgbase}-intl=$pkgver")

        install -D -m755 ${srcdir}/build/modules/intl.so ${pkgdir}/usr/lib/${pkgbase}/modules/intl.so
}

package_php70-mcrypt() {
        pkgdesc='mcrypt module for PHP'
        depends=("${pkgbase}" 'libmcrypt' 'libltdl')
        provides=("${_pkgbase}-mcrypt=$pkgver")

        install -D -m755 ${srcdir}/build/modules/mcrypt.so ${pkgdir}/usr/lib/${pkgbase}/modules/mcrypt.so
}

package_php70-odbc() {
        pkgdesc='ODBC modules for PHP'
        depends=("${pkgbase}" 'unixodbc')
        provides=("${_pkgbase}-odbc=$pkgver")

        install -D -m755 ${srcdir}/build/modules/odbc.so ${pkgdir}/usr/lib/${pkgbase}/modules/odbc.so
        install -D -m755 ${srcdir}/build/modules/pdo_odbc.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_odbc.so
}

package_php70-pgsql() {
        pkgdesc='PostgreSQL modules for PHP'
        depends=("${pkgbase}" 'postgresql-libs')
        provides=("${_pkgbase}-pgsql=$pkgver")

        install -D -m755 ${srcdir}/build/modules/pgsql.so ${pkgdir}/usr/lib/${pkgbase}/modules/pgsql.so
        install -D -m755 ${srcdir}/build/modules/pdo_pgsql.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_pgsql.so
}

package_php70-pspell() {
        pkgdesc='pspell module for PHP'
        depends=("${pkgbase}" 'aspell')
        provides=("${_pkgbase}-pspell=$pkgver")

        install -D -m755 ${srcdir}/build/modules/pspell.so ${pkgdir}/usr/lib/${pkgbase}/modules/pspell.so
}

package_php70-snmp() {
        pkgdesc='snmp module for PHP'
        depends=("${pkgbase}" 'net-snmp')
        provides=("${_pkgbase}-snmp=$pkgver")

        install -D -m755 ${srcdir}/build/modules/snmp.so ${pkgdir}/usr/lib/${pkgbase}/modules/snmp.so
}

package_php70-sqlite() {
        pkgdesc='sqlite module for PHP'
        depends=("${pkgbase}" 'sqlite')
        provides=("${_pkgbase}-sqlite=$pkgver")

        install -D -m755 ${srcdir}/build/modules/sqlite3.so ${pkgdir}/usr/lib/${pkgbase}/modules/sqlite3.so
        install -D -m755 ${srcdir}/build/modules/pdo_sqlite.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_sqlite.so
}

package_php70-tidy() {
        pkgdesc='tidy module for PHP'
        depends=("${pkgbase}" 'tidyhtml')
        provides=("${_pkgbase}-tidy=$pkgver")

        install -D -m755 ${srcdir}/build/modules/tidy.so ${pkgdir}/usr/lib/${pkgbase}/modules/tidy.so
}

package_php70-xsl() {
        pkgdesc='xsl module for PHP'
        depends=("${pkgbase}" 'libxslt')
        provides=("${_pkgbase}-xsl=$pkgver")

        install -D -m755 ${srcdir}/build/modules/xsl.so ${pkgdir}/usr/lib/${pkgbase}/modules/xsl.so
}
